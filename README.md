General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Sublime or any other text editor.
3. Have the input file in the required format.
4. Run the script using the following command `python utf8encoder.py /path/to/input`

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [UTF - 8](https://tools.ietf.org/html/rfc3629)
* [Unicode and Character Set](http://joelonsoftware.com/articles/Unicode.html)

Table of Contents
==============
----------------------------------
[TOC]

UTF - 8 Converter
===================
----------------------------------------

## Problem Description

> The program takes a path to an input file (absolute path name) as the first parameter. It will read the file as a binary file, and assume that it contains characters from Unicode's Basic Multilingual Plane (U+0000 to U+FFFF) in UTF-16 encoding (big endian), that is every 2 bytes correspond to one character and directly encode that character's Unicode code point.
> The program will encode each character in UTF-8 (between 1 and 3 bytes), and write the encoded bytes to a file called utf8encoder_out.txt.

* No Python functions or libraries that directly decode or encode characters, such as codec.encode() or codec.decode() are used in this program.
* We have assumed that the input is proper UTF-16 and encodes proper characters, so there's no need to test the input or worry about the security issues that are brought up in the UTF-8 specification.
* We have used arithmetic for computing the byte values (bitwise, integer, floating point, etc.).

For example:
## Input

![Screen Shot 2016-03-24 at 9.55.21 PM.png](https://bitbucket.org/repo/xpEAjp/images/2017825531-Screen%20Shot%202016-03-24%20at%209.55.21%20PM.png)

## Output

![Screen Shot 2016-03-24 at 9.55.33 PM.png](https://bitbucket.org/repo/xpEAjp/images/3891858526-Screen%20Shot%202016-03-24%20at%209.55.33%20PM.png)

Edit Log
=======
-----------------
## Complete Creation
> Date: 29/01/16
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 29/01/16